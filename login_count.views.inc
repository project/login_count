<?php

/**
 * @file
 * Views configuration.
 */

/**
 * Implements hook_views_data().
 */
function login_count_views_data() {
  $data['login_count']['table']['group'] = t('Login Count');
  $data['login_count']['table']['base'] = [
    'field' => 'uid',
    'title' => t('Login Count'),
    'help' => t('User login count.'),
  ];
  $data['login_count']['table']['join'] = [
    'users_field_data' => [
      'left_field' => 'uid',
      'field'      => 'uid',
    ],
  ];
  $data['login_count']['count'] = [
    'title' => t('Login Count'),
    'help'  => t("The amount of times the user has logged in."),
    'field' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'title' => t('Login Count'),
      'id' => 'numeric',
    ],
  ];
  return $data;
}
