# Login Count

Counts the amount of times a user has logged in and exposes it to Views, nothing more.

It differs from other modules by

- Not keeping a database record for every login, just increment. So there is no need to worry about purging configuration or data loss
- No privacy concern (GDPR, ...): no other user data like IP or user agent are stored
- Deletes the count related record if the user is deleted
- Views configuration does not require aggregation or relationship

## Similar projects

- Login History
- Login Tracker
